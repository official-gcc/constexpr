// { dg-do "compile" }
// { dg-options "-std=gnu++0x" }

constexpr int verysquare(int x) { return x * x; }

const double mass = 9.8;
constexpr double energy = mass * verysquare(56.6); // OK

float array[verysquare(9)];         // OK -- not C99 VLA

extern const int medium;
const int high = verysquare(medium); // OK -- dynamic initialization

enum { Max = verysquare(7) };      // OK
